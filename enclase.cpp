//LABORATORIO 12 HECHO POR RAMON COLLAZO Y RICKY DE LA VEGA
#include <QDebug>
#include <QElapsedTimer>
#include <QThread>
#include <ctime>

unsigned int oddLabor(int desde, int hasta, unsigned int *array) {
    unsigned int max = array[desde];
    for (int i=desde+1; i<=hasta; i++)
        if (array[i]>max){
            max = array[i];
        }
   return max;
}

class MyOwnThread : public QThread {
private:
   int from;
   int to;
   unsigned int *arreg;
public:
   MyOwnThread() {};
   MyOwnThread(int desde, int hasta, unsigned int *array) {from=desde; to=hasta; arreg=array;};
   virtual void run();
};

void MyOwnThread::run() {
   unsigned int  a = 0;
   qDebug() << "A thread starting from:" << from << " to:" << to;
   a = oddLabor(from,to,arreg);
   qDebug() << "Thread starting in " << from << " finished. Result:" << a;
}

void conthreads(unsigned int *array){
    MyOwnThread *T01, *T02;
    qDebug() << "Running two tasks as parallel threads...";
    // Create the threads
    T01 = new MyOwnThread(0,100000000-1,array);
    T02 = new MyOwnThread(100000000-1,200000000-1,array);
    // Start them
    T01->start();
    T02->start();
    // Wait for them to finish
    T01->wait();
    T02->wait();
}

void sinthreads(unsigned int *array){
    unsigned int tmp = array[0];
    for (unsigned int i = 1; i < 200000000; i++){
        if (array[i] > tmp){
            tmp = array[i];
        }
    }
    qDebug() << "Secuencial max: " << tmp;
}

int main(){
    srand(time(NULL));
    unsigned int *array = new unsigned int[200000000];
    for (int i=0; i<200000000 ; i++){
        array[i] = rand() % 20000000;
    }

    QElapsedTimer myTimer;

    myTimer.start();
    sinthreads(array);
    qDebug() << "Time elapsed for secuencial search: " << myTimer.elapsed() << "miliseconds";

    myTimer.start();
    conthreads(array);
    qDebug() << "Time elapsed with two threads search: " << myTimer.elapsed() << "miliseconds";

    delete []array;
    return 0;
}
