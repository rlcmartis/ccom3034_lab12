//LABORATORIO 12 HECHO POR RAMON COLLAZO Y RICKY DE LA VEGA
#include <QDebug>
#include <QElapsedTimer>
#include <QThread>
#include <QMutex>

#define INC_QTY 10000000
unsigned int sharedCount;   // the shared variable
class MyThreadIncrement : public QThread {
public:
  virtual void run();
};
void MyThreadIncrement::run()
{
  for( int count = 0; count < INC_QTY; count++ ) {
      sharedCount++;
  }
}
void testRaceCondition(){
   QElapsedTimer myTimer;
   MyThreadIncrement a,b;   // we create two threads
   sharedCount = 0;
   myTimer.start();
   a.start(); b.start();    // we run the threads
   a.wait();  b.wait();     // we wait for them to finish
   qDebug() << "Finished both threads, the sharedCount is: " << sharedCount;
   qDebug() << "Total time elapsed for the concurrent execution was : "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "================================";
}

QMutex mutex;
class MyThreadIncrementSafe : public QThread {
public:
   virtual void run();
};
void MyThreadIncrementSafe ::run() {
   for( int count = 0; count < INC_QTY; count++ ) {
       mutex.lock();       // <-- instrucción añadida
       sharedCount++;
       mutex.unlock();     // <-- instrucción añadida
   }
}
void testMutex(){
    QElapsedTimer myTimer;
    sharedCount = 0;
    MyThreadIncrementSafe  a,b;
    myTimer.start();
    a.start();   b.start();
    a.wait();    b.wait();
    qDebug() << "Finally, the sharedCount is: " << sharedCount << endl;
    qDebug() << "Finished both threads, the sharedCount is: " << sharedCount;
    qDebug() << "Total time elapsed for the concurrent execution was : "
             << myTimer.elapsed() << "miliseconds";
    qDebug() << "================================";
}

// This function performs a useless task but it takes a while
unsigned int someOddLabor(unsigned int seed) {
   unsigned int accum = seed;
   for(int i = 0; i < 10000; i++ ) {
       for(int j = 0; j < 10000; j++ )
           accum = accum * 37 + 53;
   }
   return accum;
}

class MyOwnThread : public QThread {
private:
   int mySeed;
public:
   MyOwnThread() {};
   MyOwnThread(int s) {mySeed=s;};
   virtual void run();
};

void MyOwnThread::run() {
   unsigned int  a = 0;
   qDebug() << "A thread starting with a seed:" << mySeed;
   a = someOddLabor(mySeed);
   qDebug() << "Thread with seed " << mySeed << " finished. Result:" << a;
}

void test00() {
   QElapsedTimer myTimer;
   myTimer.start();
   unsigned int result1, result2;
   qDebug() << "Running two tasks sequentially...";
   result1 = someOddLabor(25);
   qDebug() << "Result of first call:" << result1;
   result2 = someOddLabor(48);
   qDebug() << "Result of second call:" << result2;
   qDebug() << "Total time elapsed: "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "==================================";
}

void test01() {
   MyOwnThread *T01, *T02;
   QElapsedTimer myTimer;
   myTimer.start();
   qDebug() << "Running two tasks as parallel threads...";
   // Create the threads
   T01 = new MyOwnThread(25);
   T02 = new MyOwnThread(48);
   // Start them
   T01->start();
   T02->start();
   // Wait for them to finish
   T01->wait();
   T02->wait();
   qDebug() << "Total time elapsed for test01: "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "==================================";
}

void test02() {
    QElapsedTimer myTimer;
    myTimer.start();
    for (int i=0; i < 20 ; i++){
        someOddLabor(55);
    }
    qDebug() << "Time elapsed for 20 secuencial invocations: " << myTimer.elapsed() << "miliseconds";
    qDebug() << "==================================";
    return;
}

void test02B() {
   QElapsedTimer myTimer;
   myTimer.start();
   MyOwnThread *T[20];

   qDebug() << "Running 20 threads . . ";

   // create 20 threads
   for (int i=0; i<20; i++) T[i] = new MyOwnThread(i);
   // start the 20 threads


   //for (int i=0; i<20; i++) T[i]->start();
   // wait for the 20 threads to finish
   //for (int i=0; i<20; i++) T[i]->wait();

   for (int i=0; i<20; i++) {
       T[i]->start();
       T[i]->wait();
   }

   qDebug() << "Total time elapsed for test02: "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "================================";
}

int main(int argc, char *argv[]){
    //test00();
    //test01();
    //test02();
    //test02B();
    //testRaceCondition();
    //testMutex();
   return 0;
}
